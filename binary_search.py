def binary_search(needle, haystack):
    """
    Given a needle to find in a haystack, returns a tuple of `(found, index)`,
    where `found` is True if the needle was found, and `index` is the index in
    `haystack` where `needle` was found; or `found` is False if the needle was
    not found and `index` is the index in `haystack` where `needle` should be.
    """
    raise NotImplementedError()


def test_binary_search():

    # Test a full array of the numbers 0 through 10
    assert (True, 4) == binary_search(4, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    assert (True, 6) == binary_search(6, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    assert (True, 0) == binary_search(0, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    assert (True, 10) == binary_search(10, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    assert (False, 0) == binary_search(-1, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    assert (False, 11) == binary_search(11, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    assert (False, 0) == binary_search(-20, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    assert (False, 11) == binary_search(20, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    # Test an array with holes in it
    assert (True, 3) == binary_search(10, [5, 7, 9, 10, 12, 15])
    assert (False, 5) == binary_search(13, [5, 7, 9, 10, 12, 15])

    # Test an array with one element
    assert (True, 0) == binary_search(5, [5])
    assert (False, 0) == binary_search(0, [5])
    assert (False, 1) == binary_search(10, [5])

    # Test an empty array
    assert (False, 0) == binary_search(0, [])

    # Exhaustively test
    max_range = 1000
    for length in range(max_range):
        assert (False, 0) == binary_search(-1, range(length))
        for item in range(length):
            assert (True, item) == binary_search(item, range(length))
        assert (False, length) == binary_search(length + 1, range(length))

    print("All tests pass!")


if __name__ == '__main__':
    test_binary_search()
