from collections import namedtuple

# A connection between two nodes. A connection has source and dest nodes, and
# a length.
Connection = namedtuple('Path', 'source dest length'.split(' '))

# A path through a graph. A path has a length, and a list of nodes in the path.
Path = namedtuple('Path', 'length path'.split(' '))


class Graph(object):
    """
    A graph is a collection of nodes and connections.
    """
    def __init__(self, nodes=set(), connections=set()):
        self.nodes = {}
        for node in nodes:
            self.add_node(node)
        for connection in connections:
            self.connect(*connection)

    def add_node(self, node):
        """
        Add a node to the graph
        """
        self.nodes[node] = set()

    def connect(self, a, b, length=1):
        """
        Connects two nodes in the graph to each other, with the given path
        length between them.
        """
        self.nodes[a].add(Connection(a, b, length))
        self.nodes[b].add(Connection(b, a, length))

    def __getitem__(self, node):
        """
        Use ``graph[node_name]`` to get all the ``Connection``\s between two
        nodes.
        """
        return self.nodes[node]


def shortest_path(graph, start, finish):
    """
    Find the shortest path between the `start` and `finish` node of `graph`.

    If a path is found, it will return a tuple of `(length, path)`.
    `length` will be the sum of all path lengths,
    and `path` will be a list of nodes the path travels through.

    If there is no path, it returns `None`
    """
    raise NotImplementedError()


def test_shortest_path():
    # a -- b
    simple_graph = Graph('ab', {('a', 'b', 1)})
    assert (1, ['a', 'b']) == shortest_path(simple_graph, 'a', 'b')
    assert (0, ['a']) == shortest_path(simple_graph, 'a', 'a')
    assert (0, ['b']) == shortest_path(simple_graph, 'b', 'b')

    # a -- b -- c
    three_node_chain = Graph('abc', {('a', 'b', 1), ('b', 'c', 1)})
    assert (1, ['a', 'b']) == shortest_path(three_node_chain, 'a', 'b')
    assert (1, ['b', 'c']) == shortest_path(three_node_chain, 'b', 'c')
    assert (1, ['c', 'b']) == shortest_path(three_node_chain, 'c', 'b')
    assert (1, ['b', 'a']) == shortest_path(three_node_chain, 'b', 'a')
    assert (2, ['a', 'b', 'c']) == shortest_path(three_node_chain, 'a', 'c')
    assert (2, ['c', 'b', 'a']) == shortest_path(three_node_chain, 'c', 'a')

    # The sample graph from Rosetta Code
    rosetta = Graph('abcdef', {('a', 'b', 7), ('a', 'c', 9), ('a', 'f', 14),
                               ('b', 'c', 10), ('b', 'd', 15), ('c', 'd', 11),
                               ('c', 'f', 2), ('d', 'e', 6), ('e', 'f', 9)})
    assert (20, ['a', 'c', 'f', 'e']) == shortest_path(rosetta, 'a', 'e')
    assert (13, ['d', 'c', 'f']) == shortest_path(rosetta, 'd', 'f')
    assert (11, ['a', 'c', 'f']) == shortest_path(rosetta, 'a', 'f')

    # Two triangles, not connected to each other
    disconnected = Graph('abcdef', {('a', 'b'), ('b', 'c'), ('c', 'a'),
                                    ('d', 'e'), ('e', 'f'), ('f', 'd')})
    assert (1, ['a', 'b']) == shortest_path(disconnected, 'a', 'b')
    assert (1, ['f', 'd']) == shortest_path(disconnected, 'f', 'd')
    assert None is shortest_path(disconnected, 'a', 'd')
    assert None is shortest_path(disconnected, 'f', 'b')

    # A graph with a single node
    single_node = Graph('a')
    assert (0, ['a']) == shortest_path(single_node, 'a', 'a')


if __name__ == '__main__':
    test_shortest_path()
    print("All tests pass!")
