=================
Homework for Liam
=================

Implement Binary search, make a Priority queue, and then make Dijkstras
algorithm! Just for starters.

To test an implementation, do::

    $ python3 binary_search.py
