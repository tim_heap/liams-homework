class PriorityQueue(object):

    def pop(self):
        """
        Removes and returns the smallest element from the queue
        """
        raise NotImplementedError()

    def add(self, item):
        """
        Add an item to the queue
        """
        raise NotImplementedError()

    def empty(self):
        """
        Returns True if the queue is empty
        """
        raise NotImplementedError()

    def __len__(self):
        """
        Returns the length of the queue
        """
        raise NotImplementedError()


def test_priority_queue():
    queue = PriorityQueue()
    assert queue.empty
    assert len(queue) == 0

    queue.add(10)
    assert len(queue) == 1
    assert not queue.empty()
    assert queue.pop() == 10
    assert queue.empty()
    assert len(queue) == 0

    queue.add(10)
    queue.add(5)
    queue.add(20)
    assert len(queue) == 3
    assert queue.pop() == 5
    assert queue.pop() == 10
    assert queue.pop() == 20
    assert queue.empty()

    queue.add(10)
    queue.add(20)
    assert queue.pop() == 10
    queue.add(5)
    assert queue.pop() == 5
    assert queue.pop() == 20
    assert queue.empty()


if __name__ == '__main__':
    test_priority_queue()
    print('All tests pass!')
